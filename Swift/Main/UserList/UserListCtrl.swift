//
//  UserListCtrl
//  Enci
//
//  Created by Luciano Calderano on 28/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit

enum UserListType: String {
    case regionAndCity
    case cities
    case countries
    case affiliates
    case regionsOnly
}

protocol UserListDelegate : class {
    func userListDelegate(type: UserListType, id: String, name: String)
}

class UserListCtrl: MYViewController, UITableViewDataSource, UITableViewDelegate {
    class func instanceFromSb (_ sb: UIStoryboard!) -> UserListCtrl {
        return sb.instantiateViewController(withIdentifier: "UserListCtrl") as! UserListCtrl
    }
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var srchBar: UISearchBar!
    
    var subClass: List?
    var listType = UserListType.regionAndCity
    var totalArray = [JsonDict]()
    
    weak var delegate: UserListDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectClass()
        
        srchBar!.placeholder = Lng("srch")
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")

        self.loadData()
    }
    
    private func selectClass() {
        switch self.listType {
        case .countries :
            self.subClass = Country()
        case .regionAndCity, .regionsOnly :
            self.subClass = Region()
        case .cities :
            self.subClass = City()
        case .affiliates :
            self.subClass = Affiliated()
        }
        self.headerTitle = self.subClass?.title
    }
    
    private func loadData() {
        let request =  MYHttpRequest.base((self.subClass?.page)!)
        request.json = (self.subClass?.json())!
        
        request.start() { (success, response) in
            if success {
                self.totalArray = (self.subClass?.response(response))!
            }
            self.dataArray = self.totalArray
            self.tableView.reloadData()
        }
    }

    // MARK: table view
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.textLabel?.font = UIFont.mySize(20)
        cell.textLabel?.textColor = UIColor.myBlue()
        cell.textLabel?.textAlignment = .center
        
        let dic = self.dataArray[indexPath.row] as! JsonDict
        cell.textLabel?.text = dic.string((self.subClass?.keyName)!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dic = self.dataArray[indexPath.row] as! JsonDict
        self.delegate?.userListDelegate(type: self.listType,
                                        id: dic.string((self.subClass?.keyId)!),
                                        name: dic.string((self.subClass?.keyName)!))

        switch self.listType {
        case .regionAndCity :
            self.listType = .cities
            self.selectClass()
            self.subClass?.regionId = dic.string(Region().keyId)
            self.loadData()
            return
        default :
            break
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.dataArray = self.totalArray
        }
        else {
            let field = self.subClass?.keyName
            let predicate = NSPredicate.init(format: "%K contains[cd] %@", field!, srchBar.text!)
            self.dataArray = self.totalArray.filter { predicate.evaluate(with: $0) };
        }
        self.tableView.reloadData()
    }
    
/************************************/
    
    class List  {
        var title = ""
        var page = ""
        var keyName = ""
        var keyId = ""
        var regionId = ""

        func json () -> Dictionary <String, Any> {
            return [:]
        }
        func response(_ resultDict: JsonDict) -> [JsonDict] {
            return [[ : ]]
        }
    }
    
    class Country: List {
        override init() {
            super.init()
            self.title = Lng("seleNazi")
            self.page = "list-countries"
            self.keyName = "Country.name"
            self.keyId = "Country.id"
        }
        override func response(_ resultDict: JsonDict) -> [JsonDict] {
            return resultDict.array("countries") as! [JsonDict]
        }
    }
    
    class Region: List {
        override init() {
            super.init()
            self.title = Lng("seleRegi")
            self.page = "list-regions"
            self.keyName = "Region.name"
            self.keyId = "Region.id"
        }

        override func response(_ resultDict: JsonDict) -> [JsonDict] {
            return resultDict.array("regions") as! [JsonDict]
        }
    }
    
    class City: List {
        override init() {
            super.init()
            self.title = Lng("seleCity")
            self.page = "list-cities"
            self.keyName = "City.name"
            self.keyId = "City.id"
        }
        override func json () -> Dictionary <String, Any> {
            return [ "region_id": self.regionId ]
        }
        override func response(_ resultDict: JsonDict) -> [JsonDict] {
            return resultDict.array("cities") as! [JsonDict]
        }
    }
    
    class Affiliated: List {
        override init() {
            super.init()
            self.title = Lng("affiList")
            self.page = "list-enci-associations"
            self.keyName = "Association.business_name"
            self.keyId = "Association.id"
        }
        func json (filter: String) -> Dictionary <String, Any> {
            return  [ "business_name": filter ]
        }
        override func response(_ resultDict: JsonDict) -> [JsonDict] {
            return resultDict.array("enci_groups") as! [JsonDict]
        }
    }
}

