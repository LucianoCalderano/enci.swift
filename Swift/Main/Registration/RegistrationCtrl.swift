//
//  RegistrationnCtrl
//  EnciSport
//
//  Created by Luciano Calderano on 28/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit
class RegistrationnCtrl : MYViewController, RegistrationSubViewDelegate, UserListDelegate {

    @IBOutlet private var scrlUser: MYInputScrollView!
    var registrationSubView: RegistrationSubView?
    override func viewDidLoad() {
        super.viewDidLoad()
        registrationSubView = RegistrationSubView.addInputViewToInputScrollView(inputScrollView: scrlUser) as? RegistrationSubView
        registrationSubView?.inputDelegate = self
    }
    
    func selectRegionCity() {
        self.openUserList(type: .regionAndCity)
    }
    func selectCountry() {
        self.openUserList(type: .countries)
    }
    
    private func openUserList(type: UserListType) {
        let sb = UIStoryboard.init(name: "UserList", bundle: nil)
        let ctrl = UserListCtrl.instanceFromSb(sb)
        ctrl.delegate = self
        ctrl.listType = type
        self.navigationController?.show(ctrl, sender: self)
    }
    
    func userListDelegate(type: UserListType, id: String, name: String) {
        switch type {
        case .regionAndCity :
            registrationSubView?.strRegiId = id
            registrationSubView?.lblRegi.text = name
        case .cities :
            registrationSubView?.strCittId = id
            registrationSubView?.lblCitt.text = name
        case .countries :
            registrationSubView?.strPaesId = id
            registrationSubView?.lblPaes.text = name
        default:
            break
        }
    }
    
    func registrationSubViewDelegate(dicUserData: JsonDict, userType: UserType) {
        let ctrl = RegUserTypeCtrl.instanceFromSb(self.storyboard)
        ctrl.dicUser = dicUserData
        ctrl.userType = userType
        self.navigationController?.show(ctrl, sender: self)
    }
}
