//
//  MYHttp.swift
//  Enci
//
//  Created by Luciano Calderano on 07/11/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit

typealias JsonDict = Dictionary<String, Any>

struct WheelConfig {
    static var backWheelImage:UIImage?
}

class MYHttpRequest {
    struct HttpConfig {
        static let base = [
            "http://sport.enci.it/mobile/",
            "DSF7234hsdsadDSsdaskdakjhsASDhdgfgv09#62"
        ]
        static let software = [
            "http://sport.enci.it/api/",
            "SDAshadSD76320DSA-sadERYPOR3232323dsmcbghlDsa"
        ]
    }
    static let printJson = false
    class func base (_ page: String) -> MY_HttpRequest {
        return MY_HttpRequest(HttpConfig.base, page: page)
    }
    
    class func software (_ page: String) -> MY_HttpRequest {
        return MY_HttpRequest(HttpConfig.software, page: page)
    }
}



prefix operator √
prefix func √ (_ langString: String) -> String {
    return Lng(langString)
}

