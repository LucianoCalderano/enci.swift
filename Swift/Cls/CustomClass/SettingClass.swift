//
//  SettingClass.swift
//  EnciSport
//
//  Created by Luciano Calderano on 22/11/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import Foundation
enum userSettings: String {
    case userSettingsEvntCode
    case userSettingsEvntDesc
    case userSettingsDevToken
    case userSettingsDevTokenSent
    case userSettingsDeviceId
}

class SettingClass {
    private let challengeType = "challenge"
    private let challAgilDesc = "agility_dog"
    private let challAgilCode = "1"
    
    class func getChallengeType() -> String {
        return SettingClass().challengeType
    }
    
    class func getChallengeDesc() -> String {
        return SettingClass().challAgilDesc
    }

    class func getChallengeName() -> String {
        return Lng("eventName")
    }

    class func setVal(value: Any, key: userSettings) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key.rawValue)
        defaults.synchronize()
    }

    class func getValForKey(_ key: userSettings) -> String {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: key.rawValue)!
    }

    class func getObjForKey(_ key: userSettings) -> Any {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: key.rawValue) ?? ""
    }
}
