//
//  BinomiCell.swift
//  Enci
//
//  Created by Luciano Calderano on 26/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    class func dequeue (tableView: UITableView, indexPath: IndexPath) -> NewsCell {
        return tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
    }

    var dicData:JsonDict {
        set {
            self.showData(newValue.dictionary("Article"))
        }
        get {
            return [:]
        }
    }

    @IBOutlet private var titolo: MYLabel!
    @IBOutlet private var autore: MYLabel!
    @IBOutlet private var pubDat: MYLabel!
    @IBOutlet private var imageview: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.defaultCell()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.defaultCell()
    }
    
    private func defaultCell () {
        imageview?.image = UIImage.init(named: "logoBack")
        imageview?.alpha = 0.5
        autore.text = ""
    }
    
    func showData(_ dic: JsonDict) -> Void {
        titolo.text = dic.string("title")
        if dic.string("autore").isEmpty == false {
            autore.text = Lng("newsBy") + " " + dic.string("autore")
        }
        pubDat.text = Lng("newsPubl") + " " + dic.string("publishing_datetime").left(10)
        imageview?.imageFromUrl(dic.string("image"))
    }
}
