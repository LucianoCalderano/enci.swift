//
//  BinomiCtrl.swift
//  EnciSport
//
//  Created by Luciano Calderano on 28/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit
class EventsSubsBinomiCtrl: MYViewController, UITableViewDelegate, UITableViewDataSource {
    class func instanceFromSb (_ sb: UIStoryboard!) -> EventsSubsBinomiCtrl {
        return sb.instantiateViewController(withIdentifier: "EventsSubsBinomiCtrl") as! EventsSubsBinomiCtrl
    }
    
    @IBOutlet private var tableView: UITableView!
    
    var binomialId = 0
    var userId = 0
    var eventId = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // PayPal.initialize(withAppID: "APP-59C55318YH321703Y", for: ENV_LIVE)
        PayPal.initialize(withAppID: "APP-80W284485P519543T", for: ENV_SANDBOX)

        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")

        self.userId = UserClass().userId
        self.loadData()
    }
    
    private func loadData(){
        let request =  MYHttpRequest.base("book-event")
        request.json = [
            "event_id"      : self.eventId,
            "owner_id"      : self.userId,
            "binomial_id"   : self.binomialId,
            "sender_device" : "app_ios",
            "rules"         : "true",
        ]
        request.start() { (success, response) in
            if success {
                self.httpResponse(response)
            }
        }
    }
    private func httpResponse(_ resultDict: JsonDict) {
        switch (resultDict.int("status")) {
        case 0:
            self.showAlert(resultDict.string("error_msg"), message: "", okBlock: nil)
        case 1:
            self.gotoPaypal (resultDict)
        case 2:
            self.dataArray = resultDict.array("binomials_list")
            tableView.reloadData()
        default:
            break
        }
    }
    private func gotoPaypal(_ resultDict: JsonDict) {
        var array = [[String: String]]()
        for dict in resultDict.array("receivers_list") as! [JsonDict] {
            array.append(["email": dict.string("email"), "payment": dict.string("payment")])
        }
        
//        let sb = UIStoryboard.init(name: "PayPal", bundle: nil)
//        let ctrl = sb.instantiateInitialViewController() as! PayPalCtrl
        
        let sb = UIStoryboard.init(name: "PayPalSwift", bundle: nil)
        let ctrl = sb.instantiateInitialViewController() as! PayPalSwiftCtrl
        
        ctrl.dataArray = array;
        ctrl.key = "\(self.eventId).\(self.binomialId)"
        ctrl.ipnNotificationUrl = resultDict.string("ipnNotificationUrl")
        self.navigationController?.show(ctrl, sender: self)
    }
    
    // MARK: table view
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "Cell")

        cell.textLabel?.font = UIFont.mySize(16)
        cell.textLabel?.textColor = UIColor.myBlue()
        cell.detailTextLabel?.font = UIFont.mySize(14)
        cell.detailTextLabel?.textColor = UIColor.darkGray

        let dict = self.dataArray[indexPath.row] as! JsonDict
        cell.textLabel?.text = dict.string("Athlete.name") + " " +
            dict.string("Athlete.last_name") + " " +
            Lng("subsCon") + " " +
            dict.string("Dog.name") + " (" +
            dict.string("Dog.Breed.name") + ")"
        
        cell.detailTextLabel?.text = dict.string("Association.business_name")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dict = self.dataArray[indexPath.row] as! JsonDict
        self.binomialId = dict.int("Binomial.id")
        self.loadData()
    }
}

