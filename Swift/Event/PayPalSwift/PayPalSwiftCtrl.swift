//
//  PayPalSwiftCtrl
//  EnciSport
//
//  Created by Luciano Calderano on 28/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit

class PayPalSwiftCtrl: MYViewController, UITableViewDelegate, UITableViewDataSource{
    class func instanceFromSb (_ sb: UIStoryboard!) -> PayPalSwiftCtrl {
        return sb.instantiateViewController(withIdentifier: "PayPalSwiftCtrl") as! PayPalSwiftCtrl
    }
    
    @IBOutlet private var tableView: UITableView!
    
    var key = ""
    var ipnNotificationUrl = ""
    var payPalClass: PayPalClass?
    var totale = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")


        if UIApplication.shared.isIgnoringInteractionEvents == true {
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        payPalClass = PayPalClass.init(main: self)
        payPalClass?.ipnNotificationUrl = self.ipnNotificationUrl
    }
    
    // MARK: table view
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        let btn = UIButton() //.init(frame: CGRect.init(x: 5, y: 5, width: tableView.frame.size.width - 10, height: 70))
        
        btn.titleLabel?.font = UIFont.mySize(20)
        btn.setImage(UIImage.init(named: "paypal.png"), for: .normal)
        btn.setTitle("  " +  String.init(format: "%.02f", self.totale)  + " " + (payPalClass?.valuta)!, for: .normal)
        btn.backgroundColor = UIColor.myBlue()
        btn.addTarget(self, action: #selector(openPayPal), for: .touchUpInside)

        return btn
    }
    func openPayPal () {
        payPalClass?.openPayPal()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == self.dataArray.count) ? 80 : 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "Cell")
        cell.textLabel?.font = UIFont.mySize(16)
        cell.textLabel?.textColor = UIColor.myBlue()
        cell.detailTextLabel?.font = UIFont.mySize(18)
        cell.detailTextLabel?.textColor = UIColor.darkGray
        
        let dict = self.dataArray[indexPath.row] as! JsonDict
        cell.textLabel?.text = dict.string("email")
        cell.detailTextLabel?.text = dict.string("payment") + " " + (payPalClass?.valuta)!
        cell.contentView.layer.borderWidth = 0;
        totale += dict.double("payment")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

//class PayPalClass: NSObject, PayPalPaymentDelegate  {
//    var payPalConfig = PayPalConfiguration()
////    var environment:String = PayPalEnvironmentNoNetwork
//        var environment:String = PayPalEnvironmentSandbox
//    //    var environment:String = PayPalEnvironmentProduction
//    
//    let valuta = "EUR"
//    var ipnNotificationUrl = ""
//
//    var mainCtrl: EventsSubsPaypalCtrl?
//    
//    init(main: EventsSubsPaypalCtrl) {
//        super.init()
//        self.mainCtrl = main
//
//        PayPalMobile.initializeWithClientIds(forEnvironments: [
//            PayPalEnvironmentProduction: "APP-80W284485P519543T",
//            PayPalEnvironmentSandbox   : "APP-59C55318YH321703Y"
//            ])
//        PayPalMobile.preconnect(withEnvironment: environment)
//    }
//    
//    func openPayPal () {
//        payPalConfig.acceptCreditCards = false
//        payPalConfig.merchantName = "EnciSport"
//        var items = [Any]()
//        for dict in mainCtrl?.dataArray as! [JsonDict] {
//            let item = PayPalItem(name: dict.string("email"),
//                                  withQuantity: 1,
//                                  withPrice: NSDecimalNumber(string: dict.string("payment")),
//                                  withCurrency: self.valuta,
//                                  withSku: dict.string("email"))
//            items.append(item)
//        }
//        let total = PayPalItem.totalPrice(forItems: items)
//        let payment = PayPalPayment (amount: total,
//                                     currencyCode: self.valuta,
//                                     shortDescription: "EnciSport",
//                                     intent: .sale)
//        payment.items = items
//        if (payment.processable) {
//            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
//            self.mainCtrl?.present(paymentViewController!, animated: true, completion: nil)
//        }
//        else {
//            self.mainCtrl?.showAlert("Payment not processalbe", message: String(describing: payment), okBlock: nil)
//        }
//    }
//    
//    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
//        print("PayPal Payment Cancelled")
//        paymentViewController.dismiss(animated: true, completion: nil)
//    }
//    
//    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
//        print("PayPal Payment Success !")
//        do {
//            let key = mainCtrl?.key ?? "Paypal"
//            let dict = [key: completedPayment.confirmation]
//            UserDefaults.standard.setValue(dict, forKey: "Paypal")
//
//            UserDefaults.standard.synchronize()
//            
//            let json = try JSONSerialization.data(withJSONObject: completedPayment.confirmation,
//                                                  options: [])
//            print (json)
//        }
//        catch {
//            print("Json error")
//        }
//        
//        paymentViewController.dismiss(animated: true, completion: { () -> Void in
//            // send completed confirmaion to your server
//            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
//            
//        })
//    }
//}


// AYIatToRx_8mDZ8NRajycHg2BZCptt0ZdW1l0wgmOSztSAV-ESMXqtoVuH_WHxT5bDxjgeh1bVEKmcLV
// EE0mPPcm2GtectuLRaPDJOBQb94JgJCAwwivDdzMFXY7nUH8BUFjCJTq7Qj7HdyTVf3jx11zeI-lVW1I

class PayPalClass: NSObject, PayPalPaymentDelegate {
    enum PaymentStatus: Int {
        case PAYMENTSTATUS_SUCCESS
        case PAYMENTSTATUS_FAILED
        case PAYMENTSTATUS_CANCELED
    }

    let valuta = "EUR"
    var ipnNotificationUrl = ""
    var totale = 0.0
    var status = PaymentStatus.PAYMENTSTATUS_CANCELED
    var statusDesc = ""

    var mainCtrl: PayPalSwiftCtrl?
    
    init(main: PayPalSwiftCtrl) {
        super.init()
        self.mainCtrl = main
    }
    
    func openPayPal() {
        let pp = PayPal.getInst()
        pp?.delegate = self
        pp?.shippingEnabled = false
        pp?.dynamicAmountUpdateEnabled = false
        pp?.feePayer = FEEPAYER_EACHRECEIVER
        
        let payment = PayPalAdvancedPayment()
        payment.paymentCurrency = self.valuta
        payment.memo = "Memo " + self.ipnNotificationUrl
        payment.ipnUrl = self.ipnNotificationUrl
        
        payment.receiverPaymentDetails = NSMutableArray()
        for dict in (self.mainCtrl?.dataArray)! {
            let dic = dict as! JsonDict
            let detail = PayPalReceiverPaymentDetails()
            detail.description = "to: " + dic.string("email")
            detail.recipient = dic.string("email")
            detail.merchantName = "ENCI Sport";
            
            detail.subTotal = NSDecimalNumber(string: dic.string("payment"))
            detail.invoiceData = PayPalInvoiceData()
            detail.invoiceData.totalShipping = 0
            detail.invoiceData.totalTax = 0
            
            let item = PayPalInvoiceItem()
            item.totalPrice = detail.subTotal
            item.name = dic.string("email")
            detail.invoiceData.invoiceItems = NSMutableArray()
            detail.invoiceData.invoiceItems.add(item)
            payment.receiverPaymentDetails.add(detail)
        }
        pp?.advancedCheckout(with: payment)
    }
    
    func paymentSuccess(withKey payKey: String!, andStatus paymentStatus: PayPalPaymentStatus) {
        self.status = PaymentStatus.PAYMENTSTATUS_SUCCESS
        self.statusDesc = payKey

        UserDefaults.standard.setValue([(self.mainCtrl?.key)!: payKey], forKey: "PayPal")

        UserDefaults.standard.setValue(payKey, forKey: "PayPal." + (self.mainCtrl?.key)!)
        UserDefaults.standard.synchronize()

        
        print(payKey, status)
        print(PayPal.getInst().responseMessage)
    }
    
    func paymentFailed(withCorrelationID correlationID: String!) {
        self.status = PaymentStatus.PAYMENTSTATUS_FAILED
        self.statusDesc = correlationID
        print(correlationID)
        print(PayPal.getInst().responseMessage)
    }
    
    func paymentCanceled() {
        self.status = PaymentStatus.PAYMENTSTATUS_CANCELED
        print("paymentCanceled")
    }
    
    func paymentLibraryExit() {
        var title = ""
        var msg = ""
        switch self.status {
        case .PAYMENTSTATUS_SUCCESS:
            title = "Confermato !";
            msg = "Id: " + self.statusDesc
        case .PAYMENTSTATUS_FAILED:
            title = "Order failed !";
            msg = "Touch [Pay with PayPal] to try again."
        default:
            title = "Order canceled";
            msg = "Touch [Pay with PayPal] to try again."
        }
        self.mainCtrl?.showAlert(title, message: msg, okBlock: { (UIAlertAction) in
            _ = self.mainCtrl?.navigationController?.popViewController(animated: true)
        })
    }
}
