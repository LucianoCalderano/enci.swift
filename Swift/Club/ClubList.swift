//
//  ClubList
//  EnciSport
//
//  Created by Luciano Calderano on 26/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit
import MessageUI

class ViewSwipe: UIView, MFMailComposeViewControllerDelegate {
    var strPhon = ""
    var strMail = ""
    var mainCtrl: MYViewController?
    var timer = Timer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 10;
        let tap = UITapGestureRecognizer.init(target: self, action: #selector (hideView))
        self.addGestureRecognizer(tap)
    }
    
    func startTimer()  {
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(hideView), userInfo: nil, repeats: false)
    }
    
    func hideView () {
        timer.invalidate()
        UIView.animate(withDuration: 0.3, animations: {
            var rect = self.frame
            rect.origin.x = UIScreen.main.bounds.size.width
            self.frame = rect;
        })
    }
    
    @IBAction func btnPhone () {
        let s = "telprompt://" +  self.strPhon
        UIApplication.shared.openURL(URL.init(string: s)!)
    }
    
    @IBAction func btnEmail () {
        let mailComposeViewController = configuredMailComposeViewController(self.strMail)
        if MFMailComposeViewController.canSendMail() {
            mainCtrl?.present(mailComposeViewController, animated: true, completion: nil)
        }
        else {
            self.showSendMailErrorAlert("Could Not Send Email")
        }
    }
    
    func configuredMailComposeViewController(_ dest: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([dest])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if (error != nil) {
            self.showSendMailErrorAlert((error?.localizedDescription)!)
        }
        controller.popViewController(animated: true)
    }
    
    // MARK: Alert
    
    func showSendMailErrorAlert(_ title: String) {
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        mainCtrl?.present(alertController, animated: true, completion: nil)
    }
}

class ClubList: MYViewController, UIScrollViewDelegate, ClubListCellDelegate, MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, UserListDelegate {
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var txtSrch: UITextField!
    @IBOutlet private var viewSwipe: ViewSwipe!
    
    var numPage = 1
    var maxRecords = 25
    var lastPage = false
    
    var strRegionId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        viewSwipe.isHidden = true
        viewSwipe.mainCtrl = self
        
        let swipe = UISwipeGestureRecognizer.init(target: self, action: #selector (swipeRight(sender:)))
        swipe.direction = .right
        self.tableView.addGestureRecognizer(swipe)
    }
    
    func swipeRight(sender: UILongPressGestureRecognizer) {
        let p = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        let dic = self.dataArray[(indexPath?.row)!] as! JsonDict
        viewSwipe.strPhon = dic.string("phone")
        viewSwipe.strMail = dic.string("email")
        viewSwipe.startTimer()
        let cell = self.tableView.cellForRow(at: indexPath!)
        cell?.addSubview(viewSwipe)
        
        var rect = viewSwipe.frame
        rect.origin.x = -rect.size.width
        rect.origin.y = 24
        rect.size.height = (cell?.frame.size.height)! - (rect.origin.y + 2);
        viewSwipe.frame = rect
        viewSwipe.isHidden = false
        
        UIView.animate(withDuration: 0.3, animations: {
            var rect = self.viewSwipe.frame
            rect.origin.x = 0;
            self.viewSwipe.frame = rect;
        })
    }
    
    private func loadData() {
        let request =  MYHttpRequest.base("associations-list")
        request.json = [
            "page"       : numPage,
            "maxrecords" : maxRecords,
            "region_id"  : strRegionId,
            "lang_id"    : Lng("iso"),
            "src"        : txtSrch!.text!,
            "img_width"  : 80,
            "img_height" : 80,
            "img_crop"   : 2,
            "img_bg"     : "FFFFFF",
        ]
        request.start() { (success, response) in
            if success {
                self.httpResponse(response)
            }
        }
    }
    
    private func httpResponse(_ resultDict: JsonDict) {
        let arr = resultDict.array("associations")
        
        lastPage = (arr.count < maxRecords) ? true : false
        if (numPage == 1) {
            dataArray.removeAll()
        }
        dataArray += arr
        tableView.reloadData()
    }
    
    // MARK: Search
    
    @IBAction func btnSrch() {
        numPage = 1
        self.loadData()
        txtSrch?.resignFirstResponder()
    }
    
    // MARK: table view
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ClubListCell.dequeue(tableView: tableView, indexPath: indexPath)
        cell.dicData = self.dataArray[indexPath.row] as! JsonDict
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: Load next page
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (lastPage == true) {
            return
        }
        let lastRow = (self.tableView.indexPath(for: self.tableView.visibleCells.last!)?.row)! + 1
        if lastRow == self.numPage * self.maxRecords {
            numPage += 1
            self.loadData()
        }
    }
    
    // MARK: Select region
    
    override func MYNavigationBarOptionButtonTapped() {
        let sb = UIStoryboard.init(name: "UserList", bundle: nil)
        let ctrl = UserListCtrl.instanceFromSb(sb)
        ctrl.delegate = self
        ctrl.listType = .regionsOnly
        self.navigationController?.show(ctrl, sender: self)
    }
    
    func userListDelegate(type: UserListType, id: String, name: String) {
        numPage = 1
        strRegionId = id
        self.headerTitle = name
        self.loadData()
    }
    
    // MARK: delegate phone
    
    
    func didPhoneTapped(value: String) {
        let s: String = "telprompt://" + value
        UIApplication.shared.openURL(URL(string: s)!)
    }
    
    // MARK: delegate email
    
    func didEmailTapped(value: String) {
        let mailComposeViewController = configuredMailComposeViewController(value)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
        else {
            self.showSendMailErrorAlert("Could Not Send Email")
        }
    }
    
    func configuredMailComposeViewController(_ dest: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([dest])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if (error != nil) {
            self.showSendMailErrorAlert((error?.localizedDescription)!)
        }
        controller.popViewController(animated: true)
    }
    
    // MARK: Alert
    
    func showSendMailErrorAlert(_ title: String) {
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
}
