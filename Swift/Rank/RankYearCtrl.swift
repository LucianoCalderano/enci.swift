//
//  BinomiCtrl.swift
//  EnciSport
//
//  Created by Luciano Calderano on 28/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit
class RankYearCtrl: MYViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    class func instanceFromSb (_ sb: UIStoryboard!) -> RankYearCtrl {
        return sb.instantiateViewController(withIdentifier: "RankYearCtrl") as! RankYearCtrl
    }

    @IBOutlet var pickYear: UIPickerView!
    @IBOutlet var rankTypeSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rankTypeSegment.setTitle(Lng(rankTypeSegment.titleForSegment(at: 0)!), forSegmentAt: 0)
        rankTypeSegment.setTitle(Lng(rankTypeSegment.titleForSegment(at: 1)!), forSegmentAt: 1)
        self.loadData()
    }
    private func loadData(){
        let request =  MYHttpRequest.base("agility-dog/opens/trophy-years")
        request.json = [:]
        request.start() { (success, response) in
            if success {
                self.dataArray = response.array("enci_trophy_years") as! [JsonDict]
                self.pickYear.reloadAllComponents()
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.dataArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 45
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let myPickerCell = MyPickerCell()
        let dic = self.dataArray[row] as! JsonDict
        myPickerCell.text = dic.string("name")
        return myPickerCell
    }
    
    @IBAction func btnOk() {
        let row = pickYear.selectedRow(inComponent: 0)
        let dic = self.dataArray[row] as! JsonDict
        
        let ctrl = RankOptsCtrl.instanceFromSb(self.storyboard)
        ctrl.year = dic.int("year")
        self.navigationController?.show(ctrl, sender: self)
    }
}
