//
//  BinomiCell.swift
//  EnciSport
//
//  Created by Luciano Calderano on 26/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit

class ResultsCell: UITableViewCell {
    class func dequeue (tableView: UITableView, indexPath: IndexPath) -> ResultsCell {
        return tableView.dequeueReusableCell(withIdentifier: "ResultsCell", for: indexPath) as! ResultsCell
    }
    
    var dicData:JsonDict {
        set {
            self.showData(newValue.dictionary("Event"))
        }
        get {
            return [:]
        }
    }
    
    @IBOutlet private var title: MYLabel!
    @IBOutlet private var region_name: MYLabel!
    @IBOutlet private var province_name: MYLabel!
    @IBOutlet private var association_business_name: MYLabel!
    @IBOutlet private var num_subscribers: MYLabel!
    @IBOutlet private var event_start_day: MYLabel!
    @IBOutlet private var event_start_month: MYLabel!
    @IBOutlet private var infoTrophy: MYLabel!

    private func showData(_ dic: JsonDict) -> Void {

        region_name.text = dic.string("region_name")
        province_name.text = dic.string("province_name")
        association_business_name.text = dic.string("association_business_name")
        
        var subs = dic.string("num_subscribers")
        if (subs.isEmpty) {
            subs = "0"
        }
        num_subscribers.text = subs + ": " + Lng("resuPart")
        title.text = SettingClass.getChallengeName()
        
        let eventDate = dic.string("event_start").toDate(fmt: DateFormat.fmtDb.rawValue)
        
        event_start_day.text = eventDate.toString("dd")
        event_start_month.text = eventDate.toString("MMM")

        let sino =  dic.string("enci_trophy") == "No" ? Lng("NO") : Lng("SI")
        infoTrophy.text = Lng("Trophy") + ": " + sino

    }
}
