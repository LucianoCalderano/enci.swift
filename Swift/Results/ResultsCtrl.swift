//
//  BinomiCtrl.swift
//  EnciSport
//
//  Created by Luciano Calderano on 28/10/16.
//  Copyright © 2016 Kanito. All rights reserved.
//

import UIKit
class ResultsCtrl: MYViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource,UserListDelegate {
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var txtSrch: UITextField!
    
    var dicEventsMonth = [String: [JsonDict]]()
    var trialId = 0
    
    var numPage = 1
    var maxRecords = 25
    var lastPage = false
    
    var strZoneId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSrch!.placeholder = Lng("srch")
        self.loadData()
    }
    private func loadData() {
        let request =  MYHttpRequest.base("list-events")

        request.json = [
            "page"           : numPage,
            "maxrecords"     : maxRecords,
            "type"           : SettingClass.getChallengeType(),
            "challenge_type" : SettingClass.getChallengeDesc(),
            "src"            : txtSrch.text!,
            "country_id"     : "",
            "region_id"      : strZoneId,
            "city_id"        : "",
            "img_width"      : 120,
            "img_height"     : 80,
            "img_crop"       : 1,
            "img_bg"         : "FFFFFF",
            "past_events"    : 1,
        ]
        request.start() { (success, response) in
            if success {
                self.httpResponse(response)
            }
        }
    }
    
    private func httpResponse(_ resultDict: JsonDict) {
        let array = resultDict.array("events")

        lastPage = (array.count < maxRecords) ? true : false
        if (numPage == 1) {
            self.tableView.setContentOffset(CGPoint.zero, animated: false)
            self.dicEventsMonth.removeAll()
        }
        for dict in array as! [JsonDict] {
            let str = dict.string("Event.event_start")
            let annoMese = str.range(iniz: 6, fine: 10) + str.range(iniz: 3, fine: 5)
            
            var arrMese = self.dicEventsMonth[annoMese] ?? []
            arrMese.append(dict)
            dicEventsMonth[annoMese] = arrMese
        }
        
        self.dataArray = dicEventsMonth.keys.sorted {
            $0 > $1
        }
        tableView.reloadData()
    }
    
    // MARK: Search
    
    @IBAction func btnSrch() {
        numPage = 1
        self.loadData()
        txtSrch?.resignFirstResponder()
    }
    
    // MARK: table view

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataArray.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let mese = self.dataArray[section] as! String
        let arr = self.dicEventsMonth[mese]
        return arr!.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = MYLabel()
        label.backgroundColor = UIColor.myBlue()
        label.textColor = UIColor.white
        label.font = UIFont.mySize(24)
        
        let eventDate = (self.dataArray[section] as! String).toDate(fmt: "yyyyMM")
        label.text = " " + eventDate.toString("MMMM yyyy").capitalized
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ResultsCell.dequeue(tableView: tableView, indexPath: indexPath)
        cell.dicData = self.getDictAtIndex(indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dict  = self.getDictAtIndex(indexPath)
        self.trialId = dict.int("Event.id")
        
        self.loadTrial()
    }
    
    private func getDictAtIndex(_ indexPath: IndexPath) -> JsonDict {
        let month = self.dataArray[indexPath.section] as! String
        let array = self.dicEventsMonth[month]! as [JsonDict]
        return array[indexPath.row]
    }
    
    private func loadTrial () {
        let request =  MYHttpRequest.base("agility-dog/opens/list-trials")
        request.json = [
            "event_id" : self.trialId,
        ]
        request.start() { (success, response) in
            if success {
                self.httpResponseTrial(response)
            }
        }
    }

    private func httpResponseTrial (_ resultDict: JsonDict) {
        if resultDict.int ("totals") > 0 {
            self.showOpenType (resultDict.array("opens") as! Array<JsonDict>)
        }
        else {
            self.showOptions()
        }
    }
    
    private func showOpenType (_ arrOpen: Array<JsonDict>) {
        let ctrl = ResultsTypeCtrl.instanceFromSb(self.storyboard)
        ctrl.trialId = self.trialId
        ctrl.dataArray = arrOpen
        self.navigationController?.show(ctrl, sender: self)
    }

    private func showOptions () {
        let ctrl = ResultsOptsCtrl.instanceFromSb(self.storyboard)
        ctrl.trialId = self.trialId
        self.navigationController?.show(ctrl, sender: self)
    }
    
    // MARK: Load next page
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (lastPage == true) {
            return
        }
        let lastRow = (self.tableView.indexPath(for: self.tableView.visibleCells.last!)?.row)! + 1
        if lastRow == self.numPage * self.maxRecords {
            numPage += 1
            self.loadData()
        }
    }
    
    // MARK: Select region
    
    override func MYNavigationBarOptionButtonTapped() {
        let ctrl = UserListCtrl.instanceFromSb(UIStoryboard.init(name: "UserList", bundle: nil))
        ctrl.delegate = self
        ctrl.listType = .regionsOnly
        self.navigationController?.show(ctrl, sender: self)
    }

    func userListDelegate(type: UserListType, id: String, name: String) {
        numPage = 1
        strZoneId = id
        self.loadData()
    }
}

